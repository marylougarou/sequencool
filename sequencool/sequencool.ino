#include <ButtonDebounce.h>
#include <Bounce2.h>
#include <FastLED.h>
#include <EEPROM.h>

#define NUM_LEDS_1  8

CRGB leds_1[NUM_LEDS_1];
#define BRIGHTNESS  150
CRGBPalette16 currentPalette1;
TBlendType    currentBlending;

unsigned long period=1000;    // the time we need to wait
unsigned long interval= 100;  
unsigned long previousMillis;
unsigned long last_step;
unsigned long time_step;

#define ROTARY8 10
#define ROTARY16 11
#define ROTARY24 12
#define ROTARY32 22

#define OUTCLOCK 53
#define RANDOM_pin 52
ButtonDebounce ZERO (51, 100);

Bounce random_mode = Bounce();

int forward_pin = 47;
int backward_pin = 46;
Bounce forward = Bounce();
Bounce backward = Bounce();

#define clockPin 3
#define directionPin 4

// define pin of OUTPUTS
#define OUT1 23 
#define OUT2 24
#define OUT3 25
#define OUT4 26
#define OUT5 27
#define OUT6 28
#define OUT7 29
#define OUT8 30

#define BANK_LED1 39
#define BANK_LED2 40
#define BANK_LED3 41
#define BANK_LED4 42
#define BANK_LED5 43
#define BANK_LED6 44

/* #define OUT9 15 */
/* #define OUT10 16 */
/* #define OUT11 17 */
/* #define OUT12 18 */
/* #define OUT13 19 */
/* #define OUT14 20 */
/* #define OUT15 21 */
/* #define OUT16 22 */

// Define action button
ButtonDebounce RECORD (5, 100);
ButtonDebounce PLAY (6, 100);
ButtonDebounce RESET (7, 100);
ButtonDebounce ERASE (8, 100);
ButtonDebounce PAUSE (9, 100);



//define pin Button
ButtonDebounce BUTTON1 (31, 100);
ButtonDebounce BUTTON2 (32, 100);
ButtonDebounce BUTTON3 (33, 100);
ButtonDebounce BUTTON4 (34, 100);
ButtonDebounce BUTTON5 (35, 100);
ButtonDebounce BUTTON6 (36, 100);
ButtonDebounce BUTTON7 (37, 100);
ButtonDebounce BUTTON8 (38, 100);


ButtonDebounce BANK1 (A0, 100);
ButtonDebounce BANK2 (A1, 100);
ButtonDebounce BANK3 (A2, 100);
ButtonDebounce BANK4 (A3, 100);
ButtonDebounce BANK5 (A4, 100);
ButtonDebounce BANK6 (A5, 100);

#define NB_BANK 6
#define MAX_STEP 32
#define NB_CH 8
#define GATE_LENGTH 25

int clock_val = 0;
int forward_val;
int rotary8_val;
int rotary16_val;
int rotary24_val;
int rotary32_val;
int random_mode_val;

bool forward_in_val = 0;
bool backward_in_val = 0;

int state = 0;
int led = 10;
int newled;
int backgroud_color = 0;

#define YELLOW 64
#define RED 0
#define GREEN 96
#define PURPLE 192
#define BLUE 160

long debouncing_time = 10; //Debouncing Time in Milliseconds
volatile unsigned long last_micros;

int OUT[8] = {OUT1, OUT2, OUT3, OUT4, OUT5, OUT6, OUT7, OUT8};
int BANKLED[6] = {BANK_LED1, BANK_LED2, BANK_LED3, BANK_LED4, BANK_LED5, BANK_LED6};
ButtonDebounce BUTTONS[8] = {BUTTON1, BUTTON2, BUTTON3, BUTTON4, BUTTON5, BUTTON6, BUTTON7, BUTTON8};
ButtonDebounce BANK[6] = {BANK1, BANK2, BANK3, BANK4, BANK5, BANK6};

bool button_state[8];
bool button_old_state[8];

bool bank_state[6];
bool bank_old_state[6];

bool erase_state;
bool erase_old_state;

// Define the 6 banks of the sequencer, by default all seq are set to 0
bool pattern_1[NB_CH][MAX_STEP];
bool pattern_2[NB_CH][MAX_STEP];
bool pattern_3[NB_CH][MAX_STEP];
bool pattern_4[NB_CH][MAX_STEP];
bool pattern_5[NB_CH][MAX_STEP];
bool pattern_6[NB_CH][MAX_STEP];

bool overdub_pattern[NB_CH];
bool random_pattern[NB_CH];

// actual bank : 1 to 6 
int bank = 1;
// length of seq
int length_seq = 32;
// actual step : WARNING it's begin to 0
int step = -1;
int new_step;
bool flag_step = 0;
bool play_flag;
bool zero_flag;
bool step_to_low = LOW;


void setup() {

    Serial.begin(115200);  
    delay(1000);
    FastLED.addLeds<NEOPIXEL, 2>(leds_1, NUM_LEDS_1);
    FastLED.setBrightness(  BRIGHTNESS );
    currentBlending = LINEARBLEND;

    forward.attach(forward_pin,INPUT_PULLUP);
    forward.interval(25);
 
    backward.attach(backward_pin,INPUT_PULLUP);
    backward.interval(25);

    pinMode (ROTARY8, INPUT);
    pinMode (ROTARY16, INPUT);
    pinMode (ROTARY24, INPUT);
    pinMode (ROTARY32, INPUT);
    pinMode (OUTCLOCK, OUTPUT);
    pinMode (RANDOM_pin, INPUT);

    pinMode (directionPin, INPUT);

    for(int i = 0; i < NB_CH; i++){
        pinMode(OUT[i], OUTPUT);
    }


    for(int i = 0; i < NB_BANK; i++){
        pinMode(BANKLED[i], OUTPUT);
    }

    // Put of state to default value for init
    for(int i = 0; i < NB_CH; i++){
        button_state[i] = LOW; 
        button_old_state[i] = LOW;
    }


    for(int i = 0; i < NB_BANK; i++){ 
        digitalWrite(BANKLED[i], HIGH); 
        delay(50); 
        digitalWrite(BANKLED[i], LOW); 
        delay(50); 
    } 
    attachInterrupt(digitalPinToInterrupt(clockPin), clock_in, FALLING); 

  //   Get pattern from EEPROM
    Serial.print("Read pattern to eeprom : ");
    Serial.println();
    int add_index = 0;
    for  (int bk = 1; bk < NB_BANK + 1; bk++){
        Serial.print("Bank ");
        Serial.print(bk);
        for(int i = 0; i < NB_CH; i++){
            Serial.print(" CH ");
            Serial.print(i);
            Serial.print(" Pattern :  ");
            for(int j = 0; j < MAX_STEP; j++){
                bool value = 0;
                value = EEPROM.read(add_index);
                put_value_pattern(value, bk, i, j);
                Serial.print(", ");
                add_index += 1;
            }
        }
        Serial.println();
    }
}

void clock_in() {

    if( (long)(micros() - last_micros) >= debouncing_time * 1000 ) {
        last_micros = micros();
        clock_val = HIGH;
        
    }
}

/* void forward_in() { */

/*     if((long)(micros() - last_micros) >= debouncing_time * 1000) { */
/*         last_micros = micros(); */
/*         forward_in_val = HIGH; */  
/*     } */
/* } */

/* void backward_in() { */

/*     if((long)(micros() - last_micros) >= debouncing_time * 1000) { */
/*         last_micros = micros(); */
/*         backward_in_val = HIGH; */  
/*     } */
/* } */

void loop() {

    forward.update();
    backward.update();
    random_mode.update();
    
    forward_val = digitalRead(directionPin);
    rotary8_val = digitalRead(ROTARY8);
    rotary16_val = digitalRead(ROTARY16);
    rotary24_val = digitalRead(ROTARY24);
    rotary32_val = digitalRead(ROTARY32);
    random_mode_val = digitalRead(RANDOM_pin);

    

    unsigned long currentMillis = millis();
    
    RECORD.update();
    RESET.update();
    PLAY.update();
    PAUSE.update();
    ZERO.update();
    if (random_mode_val == HIGH){
           // Serial.println("RANDOMMMMM");
    }

    for(int i = 0; i < NB_CH; i++){
        ERASE.update(); 
        erase_state = ERASE.state();
    }

    for(int i = 0; i < NB_CH; i++){
        BUTTONS[i].update();
        button_state[i] = BUTTONS[i].state();
    }

    for(int i = 0; i < NB_BANK; i++){
        BANK[i].update();
        bank_state[i] = BANK[i].state();
    }

// Set lenght seq
    if(rotary8_val == HIGH){
        length_seq = 8;
        if (step >= 8) { step = step%8;}
    } else if (rotary16_val == HIGH){
        length_seq = 16;
        if (step >= 16) { step = step%16;}
    } else if (rotary24_val == HIGH){
        length_seq = 24;
        if (step >= 24) { step = step%24;}
    } else if (rotary32_val == HIGH){
        length_seq = 32;
        if (step >= 32) { step = step%32;}
    }
    
    // Add value to pattern, save pattern or switch bank
    if(RECORD.state() == HIGH){
        // if New note --> Add to pattern
        for(int i = 0; i < NB_CH; i++){
            if (button_state[i] == HIGH && button_old_state[i] == LOW){
                Serial.print("BANK :");
                Serial.print(bank);
                Serial.print("button change ");
                button_old_state[i] = HIGH;
                digitalWrite(OUT[i], HIGH);
                Serial.print("REC MODE : ");
                Serial.print("new value to pattern at step :");
                Serial.println(step);
                switch (bank){
                    case 1: 
                        pattern_1[i][step] = HIGH;
                        Serial.println("save to patter 1");
                        break;
                    case 2: 
                        pattern_2[i][step] = HIGH;
                        Serial.println("save to patter 2");
                        break;
                    case 3: 
                        pattern_3[i][step] = HIGH;
                        break;
                    case 4: 
                        pattern_4[i][step] = HIGH;
                        break;
                    case 5: 
                        pattern_5[i][step] = HIGH;
                        break;
                    case 6: 
                        pattern_6[i][step] = HIGH;
                        break;
                } 
            } else if (button_state[i] == LOW && button_old_state[i] == HIGH){
                button_old_state[i] = LOW;
                digitalWrite(OUT[i], LOW);
            }
        }
        // if bank is pressed --> Save to EEPROM
        for(int i = 0; i < NB_BANK; i++){               
            if (bank_state[i] == HIGH && bank_old_state[i] == LOW){
                bank_old_state[i] = HIGH;
                bank = i+1;
                Serial.print("Pattern save to bank : ");
                Serial.println(bank);
                int add_index = (bank - 1) * MAX_STEP * NB_CH;
                Serial.println(add_index);
                for(int i = 0; i < NB_CH; i++){
                  Serial.println();
                    for(int j = 0; j < MAX_STEP; j++){
                        bool value = get_value_pattern(bank, i, j);
                        Serial.print(value);
                        Serial.print(", ");
                        EEPROM.write(add_index, value);
                        add_index += 1;

                    }
                }
                Serial.println();
            } else if (bank_state[i] == LOW && bank_old_state[i] == HIGH){
                bank_old_state[i] = LOW;
            }
        }
    } else {
      
        // bankos
        for(int i = 0; i < NB_BANK; i++){               
            if (bank_state[i] == HIGH && bank_old_state[i] == LOW){
                bank_old_state[i] = HIGH;
                bank = i+1;
                Serial.print("New bank is : ");
                Serial.println(bank);
            } else if (bank_state[i] == LOW && bank_old_state[i] == HIGH){
                bank_old_state[i] = LOW;
            }
        }
    }

    // Overdub (on tempo mode)
    // for(int i = 0; i < NB_CH; i++){
    //     // Add note to pattern
    //     if (button_state[i] == HIGH && button_old_state[i] == LOW){
    //         button_old_state[i] = HIGH;
    //         overdub_pattern[i] = HIGH;
    //         Serial.print("button pressed: ");
    //         Serial.println(i);
    //         // Play the overdub
    //         if(play_flag == 0){
    //             Serial.print("Overdub play note on CH: ");
    //             Serial.println(overdub_pattern[i]);
    //             digitalWrite(OUT[i], overdub_pattern[i]);
    //         }
    //     } else if (button_state[i] == LOW && button_old_state[i] == HIGH){
    //         button_old_state[i] = LOW;
    //         // Play the overdub
    //         if(play_flag == 0){
    //             overdub_pattern[i] = LOW;
    //             Serial.print("Overdub play note off CH: ");
    //             Serial.println(overdub_pattern[i]);
    //             digitalWrite(OUT[i], overdub_pattern[i]);
    //         }
    //     }
    // }   
    
    // Overdub (off tempo mode)
    for(int i = 0; i < NB_CH; i++){
        // Add note to pattern
        if (button_state[i] == HIGH && button_old_state[i] == LOW){
            button_old_state[i] = HIGH;
            overdub_pattern[i] = HIGH;
            Serial.print("button pressed: ");
            Serial.println(i);
            // Play the overdub
            Serial.print("Overdub play note on CH: ");
            Serial.println(overdub_pattern[i]);
            digitalWrite(OUT[i], overdub_pattern[i]);
        } else if (button_state[i] == LOW && button_old_state[i] == HIGH){
            button_old_state[i] = LOW;
            // Play the overdub
            overdub_pattern[i] = LOW;
            Serial.print("Overdub play note off CH: ");
            Serial.println(overdub_pattern[i]);
            digitalWrite(OUT[i], overdub_pattern[i]);
            }
        }
    //}   

    // Activate play mode
    if(PLAY.state() == HIGH && PAUSE.state() == LOW && play_flag == 0){
        play_flag = 1;
        Serial.println("Play Mode");
    } else if(PLAY.state() == LOW && PAUSE.state() == HIGH && play_flag == 1){
        play_flag = 0;
        Serial.println("Pause Mode");
    }

    // Automatic clock without clock input (CAN BE COMMENTED)
    //    if((millis()-last_step) >=250){
    //        /* delay(500); */
    //        if (play_flag == 1){
    //            step += 1;
    //            if (step >= 8){
    //                step = 0;
    //            }
    //            last_step = millis();
    //            Serial.print("Step : ");
    //            Serial.println(step);
    //            update_outputs(step, bank);
    //            time_step = millis();
    //            step_to_low = HIGH;
    //        }
    //
    //    }


    
// ZEROOOOOOO
    if ((ZERO.state() == HIGH) && (zero_flag == 1)){
      zero_flag = 0;
      step = -1;
      Serial.println("zero Mode");
    }

    if ((ZERO.state() == LOW) && (zero_flag == 0)){
      zero_flag = 1;
    }

    // Clock new step 

          // OUT SYNC
          
    if (( clock_val == LOW ) && (currentMillis - previousMillis > interval)){
     digitalWrite(OUTCLOCK, LOW);
    }
       
    if (clock_val == HIGH && play_flag == 1){    
      digitalWrite(OUTCLOCK, HIGH);

      // STEPS ++ ET --
      if (forward_val == HIGH) {
            step += 1;
            newled = led + 1; 
            led = newled;

            if (step >= length_seq){
                step = 0;
            }
            if (led >= (length_seq + 11)){
                newled = 11;
                led = newled;               
            }

        } else if (forward_val == LOW ) {
            step -= 1;
            newled = led - 1; 
            led = newled;
            if (step < 0){
                step = length_seq - 1;
            }

            if (led <= 10){
                newled = (length_seq + 10);
                led = newled;               
            }

        }
        clock_val = LOW;
        step_to_low = HIGH;
        update_outputs(step, bank);
        time_step = millis();
        flag_step = 1;
    }



    // forward temporary switchie
    if ( forward.fell() ) {

        step += 1;
        newled = led + 1; 
        led = newled;

        if (step >= length_seq){
            step = 0;
        }    
        if (led >= (length_seq + 11)){
            newled = 11;
            led = newled;               
        } 
        update_outputs(step, bank);
        time_step = millis();
        step_to_low = HIGH;
        flag_step = 1;
    }

    // backward temporary switchie              
    if ( backward.fell() ) {
        step -= 1;
        newled = led - 1; 
        led = newled;

        if (step < 0){
            step = length_seq - 1;
        }
        if (led <= 10){
            newled = (length_seq + 10);
            led = newled;               
        }
        update_outputs(step, bank); 
        time_step = millis();
        step_to_low = HIGH;
        flag_step = 1;
    }


    // erase value to pattern / step by step (ERASE)
    // Serial.println(pattern_1[i][step]);
    if (erase_state == HIGH && erase_old_state == LOW){
        erase_old_state = HIGH;
        for(int i = 0; i < NB_CH; i++){
//          Serial.print("Value erase : ");
//          Serial.print(i);
//          Serial.print(" Step : ");
//          Serial.println(step);
          switch (bank){
            case 1: pattern_1[i][step] = LOW;
                    break;
            case 2: pattern_2[i][step] = LOW;
                    break;
            case 3: pattern_3[i][step] = LOW;
                    break;
            case 4: pattern_4[i][step] = LOW;
                    break;
            case 5: pattern_5[i][step] = LOW;
                    break;
            case 6: pattern_6[i][step] = LOW;
                    break;
        }
      }        
    } else if (erase_state == LOW && erase_old_state == HIGH){
        erase_old_state = LOW;
    }

    // erase value to every patterns according to bank (RESET)
    for(int i = 0; i < length_seq; i++){
        for(int j = 0; j < NB_CH; j++){
            if(RESET.state() == HIGH){   
                switch (bank){
                    case 1: pattern_1[j][i] = LOW;
                            break;
                    case 2: pattern_2[j][i] = LOW;
                            break;
                    case 3: pattern_3[j][i] = LOW;
                            break;
                    case 4: pattern_4[j][i] = LOW;
                            break;
                    case 5: pattern_5[j][i] = LOW;
                            break;
                    case 6: pattern_6[j][i] = LOW;
                            break;
                }                  
                digitalWrite(OUT[j],LOW);    
            }                                
        } 
    }

    // Put to Low ouput after 20ms
    if((millis() - time_step) >= 50 && step_to_low == HIGH){
        step_to_low = LOW;
        Serial.println("Reset");
        for (int i = 0; i <NB_CH; i++){
            // overdub_pattern[i] = LOW;  //Comment this line for overdub on tempo
            bool out_clean = 0 | overdub_pattern[i];
            digitalWrite(OUT[i], out_clean);
         }
    }
    

    if ( bank == 1 ) {
        digitalWrite (BANKLED[0], HIGH);
        digitalWrite (BANKLED[1], LOW);
        digitalWrite (BANKLED[2], LOW);
        digitalWrite (BANKLED[3], LOW);
        digitalWrite (BANKLED[4], LOW);
        digitalWrite (BANKLED[5], LOW);
    }

    if ( bank == 2 ) {
        digitalWrite (BANKLED[0], LOW);
        digitalWrite (BANKLED[1], HIGH);
        digitalWrite (BANKLED[2], LOW);
        digitalWrite (BANKLED[3], LOW);
        digitalWrite (BANKLED[4], LOW);
        digitalWrite (BANKLED[5], LOW);
    }

    if ( bank == 3 ) {
        digitalWrite (BANKLED[0], LOW);
        digitalWrite (BANKLED[1], LOW);
        digitalWrite (BANKLED[2], HIGH);
        digitalWrite (BANKLED[3], LOW);
        digitalWrite (BANKLED[4], LOW);
        digitalWrite (BANKLED[5], LOW);
    }

    if ( bank == 4 ) {
        digitalWrite (BANKLED[0], LOW);
        digitalWrite (BANKLED[1], LOW);
        digitalWrite (BANKLED[2], LOW);
        digitalWrite (BANKLED[3], HIGH);
        digitalWrite (BANKLED[4], LOW);
        digitalWrite (BANKLED[5], LOW);
    }

    if ( bank == 5 ) {
        digitalWrite (BANKLED[0], LOW);
        digitalWrite (BANKLED[1], LOW);
        digitalWrite (BANKLED[2], LOW);
        digitalWrite (BANKLED[3], LOW);
        digitalWrite (BANKLED[4], HIGH);
        digitalWrite (BANKLED[5], LOW);
    }

    if ( bank == 6 ) {
        digitalWrite (BANKLED[0], LOW);
        digitalWrite (BANKLED[1], LOW);
        digitalWrite (BANKLED[2], LOW);
        digitalWrite (BANKLED[3], LOW);
        digitalWrite (BANKLED[4], LOW);
        digitalWrite (BANKLED[5], HIGH);
    }

    if (led == 10){
        for  ( int i = 0; i < NUM_LEDS_1; i++) { 
            CRGB bgColor( 0, 20, 150); 
            fadeTowardColor( leds_1, NUM_LEDS_1, bgColor, 5);

            if (currentMillis - previousMillis > period) { // check if 1000ms passed
                previousMillis = currentMillis;
                uint16_t pos = random16( NUM_LEDS_1);
                CRGB color = CHSV( random8(), 220, 255);
                leds_1[ pos ] = color;
            }

            FastLED.show();
        }
    }

    // Update LEDs
    if(flag_step == 1){
        // Update backgroud patter LEDS
        if ((step == 0) || (step == 1) || (step == 2) || (step == 3) || (step == 4) || (step == 5) || (step == 6) || (step == 7)) {
            backgroud_color = YELLOW;
        } else if ((step == 8) || (step == 9) || (step == 10) || (step == 11) || (step == 12) || (step == 13) || (step == 14) || (step == 15)) {
            backgroud_color = RED;
        } else if ((step == 16) || (step == 17) || (step == 18) || (step == 19) || (step == 20) || (step == 21) || (step == 22) || (step == 23)) {
            backgroud_color = GREEN;
        } else if ((step == 24) || (step == 25) || (step == 26) || (step == 27) || (step == 28) || (step == 29) || (step == 30) || (step == 31)) {
            backgroud_color = PURPLE;
        }
        // Update step 
        new_step = step%8;
        for (int i = 0; i<= NUM_LEDS_1; i++){    
            if (i == new_step){
                leds_1[i] = CHSV(BLUE, 255, 220);
            } else {
                leds_1[i] = CHSV(backgroud_color, 255, 150);
            }
             }
       
        FastLED.show();
        flag_step = 0;
    }


}

void nblendU8TowardU8( uint8_t& cur, const uint8_t target, uint8_t amount)
{
    if( cur == target) return; 
    if( cur < target ) {
        uint8_t delta = target - cur;
        delta = scale8_video( delta, amount);
        cur += delta;
    } else {
        uint8_t delta = cur - target;
        delta = scale8_video( delta, amount);
        cur -= delta;
    }
}


CRGB fadeTowardColor( CRGB& cur, const CRGB& target, uint8_t amount)
{
    nblendU8TowardU8( cur.red,   target.red,   amount);
    nblendU8TowardU8( cur.green, target.green, amount);
    nblendU8TowardU8( cur.blue,  target.blue,  amount);
    return cur;
}


void fadeTowardColor( CRGB* L, uint16_t N, const CRGB& bgColor, uint8_t fadeAmount)
{
    for( uint16_t i = 0; i < N; i++) {
        fadeTowardColor( L[i], bgColor, fadeAmount);
    }
}

// function to control output
/* actual_step : Step of the pattern (0..31)
/* bank : Active bank (1..6)
 */

void update_outputs(int actual_step, int bank){
    Serial.print("Actual Step : ");
    Serial.print(step);
    Serial.print(" Actual bank : ");
    Serial.println(bank);
    if (random_mode_val == 0){
      switch(bank){
          case 1:
              for (int i = 0; i < NB_CH; i++){
                  Serial.print(" Value of output is :");
                  Serial.println(pattern_1[i][actual_step] || overdub_pattern[i]);
                  digitalWrite(OUT[i], pattern_1[i][actual_step]); //Comment this line for overdub ON tempo
                  // digitalWrite(OUT[i], pattern_1[i][actual_step] || overdub_pattern[i]); // Comment this line for overdub OFF tempo
              }
              break;
          case 2: 
              for (int i = 0; i < NB_CH; i++){ 
                  Serial.print("Value of output is :");
                  Serial.println(pattern_2[i][actual_step] || overdub_pattern[i]);
                  digitalWrite(OUT[i], pattern_2[i][actual_step]); //Comment this line for overdub ON tempo
                  // digitalWrite(OUT[i], pattern_2[i][actual_step] || overdub_pattern[i]); // Comment this line for overdub OFF tempo
              } 
              break;
           case 3:  
               for (int i = 0; i < NB_CH; i++){ 
                   Serial.print("Value of output is :"); 
                   Serial.println(pattern_3[i][actual_step] || overdub_pattern[i]); 
                  digitalWrite(OUT[i], pattern_3[i][actual_step]); //Comment this line for overdub ON tempo
                  // digitalWrite(OUT[i], pattern_3[i][actual_step] || overdub_pattern[i]); // Comment this line for overdub OFF tempo
               } 
               break; 
           case 4:  
               for (int i = 0; i < NB_CH; i++){  
                   Serial.print("Value of output is :"); 
                   Serial.println(pattern_4[i][actual_step] || overdub_pattern[i]); 
                  digitalWrite(OUT[i], pattern_4[i][actual_step]); //Comment this line for overdub ON tempo
                  // digitalWrite(OUT[i], pattern_4[i][actual_step] || overdub_pattern[i]); // Comment this line for overdub OFF tempo
               }  
               break; 
           case 5:  
               for (int i = 0; i < NB_CH; i++){  
                   Serial.print("Value of output is :"); 
                   Serial.println(pattern_5[i][actual_step] || overdub_pattern[i]); 
                  digitalWrite(OUT[i], pattern_5[i][actual_step]); //Comment this line for overdub ON tempo
                  // digitalWrite(OUT[i], pattern_5[i][actual_step] || overdub_pattern[i]); // Comment this line for overdub OFF tempo
               }  
                 
               break; 
           case 6: 
               for (int i = 0; i < NB_CH; i++){  
                   Serial.print("Value of output is :"); 
                   Serial.println(pattern_6[i][actual_step] || overdub_pattern[i]); 
                  digitalWrite(OUT[i], pattern_6[i][actual_step]); //Comment this line for overdub ON tempo
                  // digitalWrite(OUT[i], pattern_6[i][actual_step] || overdub_pattern[i]); // Comment this line for overdub OFF tempo
               }  
               break; 
      }
    } else {
      random_generator();
      for (int i = 0; i < NB_CH; i++){
        Serial.print(" Value of RANDOM output is :");
        Serial.println(random_pattern[i]);
        digitalWrite(OUT[i], random_pattern[i]); //Comment this line for overdub ON tempo
        }
    }
}

bool get_value_pattern(int bank, int channel, int step){
    bool value =0;
    switch (bank){
        case 1:
            value = pattern_1[channel][step];
            break;
        case 2:
            value = pattern_2[channel][step];
            break;
        case 3:
            value = pattern_3[channel][step];
            break;
        case 4:
            value = pattern_4[channel][step];
            break;
        case 5:
            value = pattern_5[channel][step];
            break;
        case 6:
            value = pattern_6[channel][step];
            break;
    }
    return value;
}

void put_value_pattern(bool value, int bank, int channel, int step){
    switch (bank){
        case 1:
            pattern_1[channel][step] = value;
            Serial.print(pattern_1[channel][step]);
            break;
        case 2:
            pattern_2[channel][step] = value;
            Serial.print(pattern_2[channel][step]);
            break;
        case 3:
            pattern_3[channel][step] = value;
            Serial.print(pattern_3[channel][step]);
            break;
        case 4:
            pattern_4[channel][step] = value;
            Serial.print(pattern_4[channel][step]);
            break;
        case 5:
            pattern_5[channel][step] = value;
            Serial.print(pattern_5[channel][step]);
            break;
        case 6:
            pattern_6[channel][step] = value;
            Serial.print(pattern_6[channel][step]);
            break;
    }
}

void random_generator(){
  for (int i = 0; i<8; i++){
    randomSeed(micros()); // No unconnected analog pins available, seed with millis()
    bool outcome = random(0, 1024) >= (0.9 * 1024); // Value of proba between 0 and 1
    random_pattern[i] = outcome;
  }
}
