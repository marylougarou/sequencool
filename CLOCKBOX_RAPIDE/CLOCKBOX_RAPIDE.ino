// ça c'est une clockbox designed par YMNK music (https://www.ymnkmusic.com/diy-synths/clockbox/)
// que j'aimerais reprendre et "augmenter"
// fonctionne avec la librairie uClock.h (https://github.com/midilab/uClock)
// Je l'ai testée avec juste un pot, un switch et une LED et ça fonctionne plutôt bien 
// comme le potentiometre est un peu instable et que j'aimerais des tempos précis qui ne changent pas, je pense créer deux Clockboxes, une lente (10 à 220 BPM) et une rapide (de 220 à 430 BPM)
// ici c'est la rapide

#include <SevenSegmentTM1637.h>


#include <uClock.h>
#define ANALOG_SYNC_RATIO 4

const byte PIN_CLK = 38;
const byte PIN_DIO = 39;   
SevenSegmentTM1637    display(PIN_CLK, PIN_DIO);

#define tempoPot A0
int tempoPot_read;
int tempo;
int newtempo;
int tempo_div;

bool currentState = false;
bool currentSwitchState = false;
bool needsToSendMidiStart = false;

const byte pinCount = 4;
byte digitalPinOut[pinCount] = {3,5,7,9}; //Define analog clock outputs here

void clockOutput96PPQN(uint32_t* tick) {
  if (needsToSendMidiStart) {
    needsToSendMidiStart = false;
    Serial.write(0xFA);
  }
  Serial.write(0xF8);
}

void clockOutput32PPQN(uint32_t* tick) {
  if (currentState) {
    if ((*tick % ANALOG_SYNC_RATIO ) == 0) {
      sendDigitalOut(true);
    } else {
      sendDigitalOut(false);
    }
  }
}

void sendDigitalOut(bool state1) {
  byte pinState = state1 ? HIGH : LOW;
  
  for (byte i = 0; i < pinCount; i++) {
    digitalWrite(digitalPinOut[i], pinState);
  }
}

void setup() {

  Serial.begin(115200);
  uClock.init();

  pinMode(2, INPUT);
  pinMode(tempoPot, INPUT);



  for (byte i = 0; i < pinCount; i++) {
    pinMode(digitalPinOut[i], OUTPUT);
  }
  

  uClock.init();
  
  uClock.setClock96PPQNOutput(clockOutput96PPQN);
  uClock.setClock32PPQNOutput(clockOutput32PPQN);
  uClock.setTempo(96);

  uClock.start();

  display.begin();            // initializes the display
  display.setBacklight(100);  // set the brightness to 100 %
  display.clear(); 
  display.print("LETS GO BIRDS");
}

void toggleStartStop() {
  if (currentState) {
    Serial.write(0xFC);
    sendDigitalOut(false);
    currentState = false;
    
  } else {
    uClock.stop();
    delay(20);
    currentState = true;
    needsToSendMidiStart = true;
    uClock.start();  
  }
}

void loop() {

 tempoPot_read = analogRead(tempoPot);
 tempo_div = map(tempoPot_read, 0, 1023, -15, 205); // en gros ça ira de 15 Battements par Minute (1 temps toutes les 3 secondes à 240 battements, càd 4 battements par seconde)
   
// REPARTITIONSSSSS
// Alors bizarrement je me suis rendue compte que c'est le tempo 30 qui donne 60 BPM

   if ( (tempo_div >= -15) && (tempo_div < -5) ) {
    newtempo = 110;
    tempo = newtempo;
    display.print(" 220");
    }
    
    if ( (tempo_div >= -5) && (tempo_div < 5) ) {
    newtempo = 115;
    tempo = newtempo;
    display.print(" 230");
    }
    
    if ( (tempo_div >= 5) && (tempo_div < 15) ) {
    newtempo = 120;
    tempo = newtempo;
    display.print(" 240");
    }
    
    if ( (tempo_div >= 15) && (tempo_div < 25) ) {
    newtempo = 125;
    tempo = newtempo;
    display.print(" 250");
    }
    
    if ( (tempo_div >= 25) && (tempo_div < 35) ) {
    newtempo = 130;
    tempo = newtempo;
    display.print(" 260");
    }
    if ( (tempo_div >= 35) && (tempo_div < 45) ) {
    newtempo = 135;
    tempo = newtempo;
    display.print(" 270");
    }
    if ( (tempo_div >= 45) && (tempo_div < 55) ) {
    newtempo = 140;
    tempo = newtempo;
    display.print(" 280");
    }
    if ( (tempo_div >= 55) && (tempo_div < 65) ) {
    newtempo = 145;
    tempo = newtempo;
    display.print(" 290");
    }
    if ( (tempo_div >= 65) && (tempo_div < 75) ) {
    newtempo = 150;
    tempo = newtempo;
    display.print(" 300");
    }
    if ( (tempo_div >= 75) && (tempo_div < 85) ) {
    newtempo = 155;
    tempo = newtempo;
    display.print(" 310");
    }
    if ( (tempo_div >= 85) && (tempo_div < 95) ) {
    newtempo = 160;
    tempo = newtempo;
    display.print(" 320");
    }
    if ( (tempo_div >= 95) && (tempo_div < 105) ) {
    newtempo = 165;
    tempo = newtempo;
    display.print(" 330");
    }
    if ( (tempo_div >= 105) && (tempo_div < 115) ) {
    newtempo = 170;
    tempo = newtempo;
    display.print(" 340");
    }
    if ( (tempo_div >= 115) && (tempo_div < 125) ) {
    newtempo = 175;
    tempo = newtempo;
    display.print(" 350");
    }
    if ( (tempo_div >= 125) && (tempo_div < 135) ) {
    newtempo = 180;
    tempo = newtempo;
    display.print(" 360");
    }
    if ( (tempo_div >= 135) && (tempo_div < 145) ) {
    newtempo = 185;
    tempo = newtempo;
    display.print(" 370");
    }
    if ( (tempo_div >= 145) && (tempo_div < 155) ) {
    newtempo = 190;
    tempo = newtempo;
    display.print(" 380");
    }
    if ( (tempo_div >= 155) && (tempo_div < 165) ) {
    newtempo = 195;
    tempo = newtempo;
    display.print(" 390");
    }
    if ( (tempo_div >= 165) && (tempo_div < 175) ) {
    newtempo = 200;
    tempo = newtempo;
    display.print(" 400");
    }
    if ( (tempo_div >= 175) && (tempo_div < 185) ) {
    newtempo = 205;
    tempo = newtempo;
    display.print(" 410");
    }
    if ( (tempo_div >= 185) && (tempo_div < 195) ) {
    newtempo = 210;
    tempo = newtempo;
    display.print(" 420");
    }
    if ( (tempo_div >= 195) && (tempo_div <= 205) ) {
    newtempo = 215;
    tempo = newtempo;
    display.print(" 430");
    }
    
    // idée : faire 2 clock box, une rapide et une lente
    

   Serial.println(tempo);
 
  

  uClock.setTempo(tempo);
  int switchState = digitalRead(2);

  if ((switchState == HIGH) && !currentSwitchState) {
    toggleStartStop();
    currentSwitchState = true;
  }
  
  if (switchState == LOW) {
    currentSwitchState = false;
  }

  delay(40);
}
